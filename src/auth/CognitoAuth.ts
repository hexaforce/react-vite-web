const CognitoAuth = {
  isAuthenticated: false,
  signin(callback: VoidFunction) {
    CognitoAuth.isAuthenticated = true
    setTimeout(callback, 100) // fake async
  },
  signout(callback: VoidFunction) {
    CognitoAuth.isAuthenticated = false
    setTimeout(callback, 100)
  },
}

export { CognitoAuth }
