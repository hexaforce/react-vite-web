import { Navigate, useLocation } from 'react-router-dom'
import { useAuth } from 'src/auth/AuthProvider'

const PrivateRoute = ({ children }: { children: JSX.Element | JSX.Element[] }) => {
  let auth = useAuth()
  let location = useLocation()

  if (!auth.user) {
    return <Navigate to='/login' state={{ from: location }} replace />
  }

  return children
}

export { PrivateRoute }
