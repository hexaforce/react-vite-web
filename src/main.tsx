import React from 'react'
import ReactDOM from 'react-dom/client'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import Routes from 'src/Routes'
import { AuthProvider } from 'src/auth/AuthProvider'
import AuthStatus from 'src/auth/AuthStatus'
import 'src/main.css'
import { store } from 'src/utils/redux/store'

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <AuthProvider>
          <header id="header">
            <div id="logo">
              <h1>CoolLogo</h1>
              <AuthStatus />
            </div>
          </header>
          <main>
            <div className="innertube">
              <h1>Main</h1>
              <Routes />
            </div>
          </main>
          <nav id="nav">
            <div className="innertube" style={{}}>
              <h1>Heading</h1>
              <ul>
                <li><a href="#">Link 1</a></li>
                <li><a href="#">Link 2</a></li>
                <li><a href="#">Link 3</a></li>
                <li><a href="#">Link 4</a></li>
                <li><a href="#">Link 5</a></li>
              </ul>
              <h1>Heading</h1>
              <ul>
                <li><a href="#">Link 1</a></li>
                <li><a href="#">Link 2</a></li>
                <li><a href="#">Link 3</a></li>
                <li><a href="#">Link 4</a></li>
                <li><a href="#">Link 5</a></li>
              </ul>
            </div>
          </nav>
        </AuthProvider>
      </BrowserRouter>
    </Provider>
  </React.StrictMode>,
)
