import { Route, Routes } from 'react-router-dom'
import Layout from 'src/components/Layout'
import LoginPage from 'src/page/LoginPage'
import ProtectedPage from 'src/page/ProtectedPage'
import PublicPage from 'src/page/PublicPage'

export default () => {
  return (
    <Routes>
      <Route element={<Layout />}>
        <Route path='/' element={<PublicPage />} />
        <Route path='/login' element={<LoginPage />} />
        <Route path='/protected' element={<ProtectedPage />} />
      </Route>
    </Routes>
  )
}
