import { dice, hello, random } from '@hexaforce/data-integration-module'
import { useEffect, useState } from 'react'
import { PrivateRoute } from 'src/auth/PrivateRoute'

const GRAPHQL_ENDPOINT = import.meta.env.VITE_GRAPHQL_ENDPOINT

export default () => {
  const [count, setCount] = useState(0)
  useEffect(() => {
    // console.log(GRAPHQL_ENDPOINT)
  }, [])
  return (
    <PrivateRoute>
      <h3>Protected</h3>
      <button onClick={() => setCount((count) => count + 1)}>count is {count}</button>
      <button onClick={() => hello(GRAPHQL_ENDPOINT)}>hello</button>
      <button onClick={() => random(GRAPHQL_ENDPOINT)}>random</button>
      <button onClick={() => dice(GRAPHQL_ENDPOINT)}>dice</button>
    </PrivateRoute>
  )
}
