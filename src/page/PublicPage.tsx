import { useDispatch, useSelector } from 'react-redux'
import { decrement, increment } from 'src/utils/redux/reducers'
import type { RootState } from 'src/utils/redux/store'

export default () => {
  const count = useSelector((state: RootState) => state.counter.value)
  const dispatch = useDispatch()

  return (
    <div>
      <button aria-label='Increment value' onClick={() => dispatch(increment())}>
        Increment
      </button>
      <span>{count}</span>
      <button aria-label='Decrement value' onClick={() => dispatch(decrement())}>
        Decrement
      </button>

    </div>
  )
}
